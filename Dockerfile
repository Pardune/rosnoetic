FROM ubuntu:20.04

//ENV

// noetic repo Setup, and install
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver hkp:///ha.pool.sks-keyservers.net:80 --rec-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
RUN apt update
RUN apt install ros-noetic-desktop-full

// make setupscript permanently available
RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
source /opt/ros/noetic/setup.bash

// install additional packages
//RUN apt install ros-noetic-PACKAGE

// catkin workspace
RUN mkdir -p ~/catkin_ws/src
RUN cd ~/catkin_ws/
RUN catkin_make
